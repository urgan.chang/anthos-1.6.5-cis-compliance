#! /bin/bash

set -ex

#
# Script 1 before resize disk
#

# Item 1
function update_login_def() {
  sudo sed -i "s/^PASS_MAX_DAYS.*/PASS_MAX_DAYS\t90/g" /etc/login.defs
  sudo sed -i "s/^PASS_MIN_DAYS.*/PASS_MIN_DAYS\t7/g" /etc/login.defs
  sudo sed -i "s/^PASS_WARN_AGE.*/PASS_WARN_AGE\t14/g" /etc/login.defs
  sudo sed -i "s/^#PASS_MIN_LEN.*/PASS_MIN_LEN\t12/g" /etc/login.defs
}

# Item 2
function update_pwquality() {
  sudo sed -i "s/^  minlen.*/  minlen = 12/g" /etc/security/pwquality.conf
  sudo sed -i "s/^  dcredit.*/  dcredit = -1/g" /etc/security/pwquality.conf
  sudo sed -i "s/^  lcredit.*/  lcredit = -1/g" /etc/security/pwquality.conf
  sudo sed -i "s/^  ucredit.*/  ucredit = -1/g" /etc/security/pwquality.conf
  sudo sed -i "s/^  ocredit.*/  ocredit = -1/g" /etc/security/pwquality.conf
  sudo sed -i "s/^  difok.*/  difok = 3/g" /etc/security/pwquality.conf
}

# Item 3
function update_pam_rule() {
  sudo sed -i "s/auth\trequired.*/auth\trequired      pam_faillock.so preauth silent audit deny=3 unlock_time=900/g" /etc/pam.d/common-auth
  echo -e "auth\t[default=die] pam_faillock.so authfail audit deny=3 unlock_time=900" sudo tee -a /etc/pam.d/common-auth

  sudo sed -i "s/password\trequisite.*/password\trequisite     pam_pwquality.so try_first_pass local_users_only retry=3 authtok_type=/g" /etc/pam.d/common-password
  echo -e "password\tsufficient    pam_unix.so sha512 shadow nullok try_first_pass use_authtok remember=24" | sudo tee -a /etc/pam.d/common-password
}

# Item 8
function disable_nobody() {
  sudo passwd -l nobody
}

# Item 9
function update_user_shell() {
  sudo usermod --shell /usr/sbin/nologin sync
  sudo usermod --shell /usr/sbin/nologin lxd
  sudo usermod --shell /usr/sbin/nologin pollinate
}

# Item 15
function update_sshd_config() {
  sudo sed -i 's/^#PermitEmptyPasswords no/PermitEmptyPasswords no/g' /etc/ssh/sshd_config
  sudo sed -i 's/ClientAliveInterval.*/ClientAliveInterval 300/g' /etc/ssh/sshd_config
  sudo sed -i 's/ClientAliveCountMax.*/ClientAliveCountMax 3/g' /etc/ssh/sshd_config
  echo 'DenyGroups denysshg' | sudo tee -a /etc/ssh/sshd_config

  # sudo systemctl restart sshd
}

# Item 16
function update_user_add() {
  sudo sed -i 's/^# INACTIVE=-1/INACTIVE=30/g' /etc/default/useradd
}

# Item 17
function update_tmout() {
  sudo sed -i 's/TMOUT=.*/TMOUT=900/g' /etc/profile

  cat <<EOF | sudo tee /etc/profile.d/tmout.sh
#!/bin/bash
TMOUT=900
readonly TMOUT
export TMOUT
EOF

  sudo chmod +x /etc/profile.d/tmout.sh
}

# Item 21
function update_rsyslog() {
  sudo apt-get update -y && \
  sudo apt-get install -y rsyslog && \
  sudo apt-get clean && \
  sudo apt-get autoclean

  cat <<EOF | sudo tee /etc/rsyslog.d/50-default.conf
*.info;mail.none;authpriv.none;cron.none                /var/log/messages
authpriv.*                                              /var/log/secure
mail.*                                                  -/var/log/maillog
cron.*                                                  /var/log/cron
*.emerg                                                 :omusrmsg:*
uucp,news.crit                                          /var/log/spooler
local7.*                                                /var/log/boot.log
EOF

  sudo systemctl restart rsyslog

  cat <<EOF | sudo tee /etc/logrotate.d/bootlog
/var/log/boot.log
{
    missingok
    compress
    delaycompress
    daily
    copytruncate
    rotate 7
    notifempty
}
EOF

cat <<EOF | sudo tee /etc/logrotate.d/syslog
/var/log/cron
/var/log/maillog
/var/log/secure
/var/log/spooler
{
    missingok
    compress
    delaycompress
    sharedscripts
    postrotate
        /bin/kill -HUP `cat /var/run/rsyslogd.pid 2> /dev/null` 2> /dev/null || true
    endscript
}
EOF
}

# Item22 Pass

# Item 24 Created by new template

# Item 27
function set_umark() {
  echo 'umask 027' | sudo tee -a /etc/profile
  echo 'umask 027' | sudo tee -a /etc/bash.bashrc
}

# Item 28
function enable_auditd() {
  sudo systemctl enable auditd
  sudo systemctl list-unit-files | grep enabled | grep auditd
}

# Item 29
function update_audit_rule() {
  echo '-a always,exit -F arch=b64 -S adjtimex -S settimeofday -S stime -k time-change' | sudo tee -a /etc/audit/audit.rules
  echo '-a always,exit -F arch=b64 -S clock_settime -k time-change -w /etc/localtime -p wa -k time-change' | sudo tee -a /etc/audit/audit.rules

  # Item 30
  echo '-a always,exit -F arch=b64 -S creat -S open -S openat -S truncate -S ftruncate -F exit=-EACCES -F auid>=500 -F auid!=4294967295 -k access' \
  | sudo tee -a /etc/audit/audit.rules

  echo '-a always,exit -F arch=b64 -S creat -S open -S openat -S truncate -S ftruncate -F exit=-EPERM -F auid>=500 -F auid!=4294967295 -k access' \
  | sudo tee -a /etc/audit/audit.rules
}

# Main
update_login_def
update_pwquality
update_pam_rule
disable_nobody
update_user_shell
update_sshd_config
update_user_add
update_tmout
update_rsyslog
set_umark
enable_auditd
update_audit_rule
