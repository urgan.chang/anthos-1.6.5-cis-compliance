--- 10_linux.orig	2020-10-21 02:41:04.126850881 +0000
+++ 10_linux	2020-10-21 02:43:09.369182488 +0000
@@ -131,7 +131,12 @@
       fi
       echo "menuentry '$(echo "$title" | grub_quote)' ${CLASS} \$menuentry_id_option 'gnulinux-$version-$type-$boot_device_id' {" | sed "s/^/$submenu_indentation/"
   else
+      OLD_CLASS="$CLASS"
+      if [ "$(echo "$os" | grub_quote)" = "Ubuntu" ]; then
+          CLASS="$CLASS  --unrestricted"
+      fi
       echo "menuentry '$(echo "$os" | grub_quote)' ${CLASS} \$menuentry_id_option 'gnulinux-simple-$boot_device_id' {" | sed "s/^/$submenu_indentation/"
+      CLASS="$OLD_CLASS"
   fi
   if [ "$quick_boot" = 1 ]; then
       echo "	recordfail" | sed "s/^/$submenu_indentation/"