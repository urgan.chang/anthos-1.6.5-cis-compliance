# Cathay CIS-Compliance in Ubuntu-18.04 LTS on Anthos 1.6.5

[TOC]

## Prepare OVA

- gke-on-prem-admin-appliance-vsphere-1.6.5-gke.0.ova
- ubuntu-18.04.5-live-server-amd64.iso

## Customized Requirements

- /tmp 需在獨立磁區
- /var/tmp 需在獨立磁區 or *mount*到/tmp
- /dev/sda1 需 60GB

## Create OS image template

1. 在上傳[gke-on-pＳrem-admin-appliance-vsphere-1.6.5-gke.0.ova] 至 vSphere
2. Deploy OVF template


   | ![24] |
   | ------- |
3. choose local file with [gke-on-prem-admin-appliance-vsphere-1.6.5-gke.0.ova]


   | ![1] |
   | ------ |
4. choose resources based on current environment ( [] 為UT環境 設定值 )

   1. choose vcenter.datacenter: [DataCenter]
   2. choose vcenter.cluster: [DEV_Cluster]
   3. choose vcenter.folder: [APIM]


      | ![2] |
      | ------ |
   4. validate on-prem resources


      | ![3] |
      | ------ |
   5. choose vcenter.datastore


      | ![4] |
      | ------ |
   6. choose internet-enabled network: [DEV_88.249.28~31.X]


      | ![5] |
      | ------ |
   7. set password-only-once for web-console login


      | ![6] |
      | ------ |
   8. validate all configuration, and click FINISH if correct


      | ![7] |
      | ------ |
5. Follow anthos-on-prem guide to create the admin cluster .yaml file
   Read more: [admin-cluster.yaml](../Anthos/admin-cluster/admin-cluster.yaml)
   Read more: [admin-cluster-hostconfig.yaml](../Anthos/admin-cluster/admin-cluster-hostconfig.yaml)
   Read more: [admin-creds.yaml](../Anthos/admin-cluster/admin-creds.yaml)
6. Run Anthos check-config to upload os-template to vCenter

   ```shell
   gkectl check-config --config admin-cluster.yaml
   ```
7. 等驗證過程完成, 即可在 **vcenter.folder** 見到原生版本的VM template: "**gke-on-prem-ubuntu-1.6.5-gke.0**"


   | ![25] |
   | ------- |

## Create VM

從上一步所上傳的template建立VM, 於 Customize vApp properties 設定一次性密碼


| ![12] |
| ------- |

### 改變 VM 組態

- disk1: 主系統磁區 (60GB)
- disk2: 暫存用碟區 (20GB)
- 新增 CD/DVD device, 並掛載 [ubuntu-18.04.5-live-server-amd64.iso]
- 調整 開機延遲時間為 10 秒


  | ![14] |
  | ------- |
- 開機時按下 ESC 進入開機選單，使用 CD/DVD 開機。(＊若一開始為成功進入變成正常開機, 需重來)


  | ![15] |
  | ------- |
- 按下 Tab 將游標移至 Help，於選單中點擊 Enter Shell。


  | ![16] |
  | ------- |

### Modify Disk partition

Origin: /dev/sda14 -> /dev/sda15 -> /dev/sda1


| /dev/sda14 | /dev/sda15 | /dev/sda1 |
| -----------: | -----------: | ----------: |
|            |  /boot/efi |         / |
|         4M |       106M | Residuals |

Target: /dev/sda14 -> /dev/sda15 -> /dev/sda16 -> /dev/sda1


| /dev/sda14 | /dev/sda15 | /dev/sda16 | /dev/sda1 |
| -----------: | -----------: | -----------: | ----------: |
|            |  /boot/efi |       /tmp |         / |
|         4M |       106M |        4GB | Residuals |

### Operation Steps

1. 準備作/dev/sda的鏡像, 切 /dev/sdc 的 parttition
2. 將 /dev/sdb1 的空間作格式化

   ```shell
   gdisk /dev/sdb
   n
   enter
   enter
   enter
   w
   y

   mkfs.ext4 /dev/sdb1
   ```
3. 取得 /dev/sda1 的 [PARTUUID]

   ```shell
   blkid | grep cloudimg-rootfs | sed -rn 's/^.+PARTUUID="([^"]+)"/\1/p' | tr -d '-'
   ```
4. 備份 /dev/sda1 整個磁區到 /dev/sdb1 所 mount的 Folder

   ```shell
   mkdir /dd_files
   mount /dev/sdb1 /dd_files
   dd if=/dev/sda1 of=/dd_files/sda1.img bs=10M status=progress
   ```
5. 刪除 /dev/sda1 磁區
6. 新增 /dev/sda16 磁區 並設定大小為 4 GiB 及格式化
7. 重新產生磁區 /dev/sda16 的 PARTUUID
8. 為 /dev/sda16 添加 label tmp

   ```shell
   gdisk /dev/sda
   d
   1
   w
   y
   ```

   ```shell
   gdisk /dev/sda
   n
   16
   enter
   +4G
   enter
   w
   y
   ```

   ```shell
   gdisk /dev/sda16
   x
   c
   16
   R
   w
   y
   ```

   ```shell
   mkfs.ext4 /dev/sda16
   tune2fs -U $(uuidgen) /dev/sda16
   e2label /dev/sda16 tmp
   ```
9. 新增 /dev/sda1 磁區 並設定大小到磁區最後面

   ```shell
   gdisk dev/sda
   n
   1
   enter
   enter
   enter
   w
   y
   ```
10. 將 step 4.的 sda1.img 做還原回 /dev/sda1
11. 為 /dev/sda1 做磁區空間驗證
    [PARTUUID][1.6.5] = 22f23426-4aa3-44cf-b377-ad431eeda522

    ```shell
    dd if=/dd_files/sda1.img of=/dev/sda1 bs=10M status=progress
    e2fsck /dev/sda1
    ```
12. 將 step 3. 所取的 PARTUUID 設回 /dev/sda1

    ```shell
    gdisk /dev/sda
    x
    c
    1
    [PARTUUID]
    w
    y
    ```
13. 修正 grub

    ```shell
    mount /dev/sda1 /mnt
    mount --bind /dev /mnt/dev
    mount --bind /proc /mnt/proc
    mount --bind /sys /mnt/sys
    mkdir /efi
    mount /dev/sda15 /efi
    mount --bind /efi /mnt/boot/efi
    chroot /mnt
    ```
14. 更新 /etc/fstab

    ```txt
    LABEL=cloudimg-rootfs   /       ext4   defaults        0 1
    LABEL=UEFI      /boot/efi       vfat    umask=0077      0 1
    LABEL=tmp       /tmp    ext4    nodev,nosuid    0 0
    /tmp            /var/tmp        none    nodev,nosuid,bind       0 0
    ```
15. 關機

    ```shell
    exit
    shutdown -h now
    ```
16. 修改設定

- 移除 disk2 & DVD-ROM
- 使用預設開機密碼做首次登入

## Modify configuration

### 為開啟網路遠端登入

1. 設定 /etc/ssh/sshd_config

```shell
sudo sed -i 's/PasswordAuthentication no/PasswordAuthentication yes/' /etc/ssh/sshd_config
sudo systemctl restart sshd
```

2. 設定 /etc/netplan/00-cloud-init.yaml

- IP: [內部連線用IP]
- NETMASK: [網路遮罩]
- GATEWAY: [vCenter閘道]
- NAMESERVERS: [vCenter內部DNS]

```yaml
network:
  version: 2
  ethernets:
    ens192:
      addresses: [IP/NETMASK]
      gateway4: GATEWAY
      nameservers:
        addresses: [NAMESERVER01]
      dhcp4: no
```

```shell
sudo netplan apply
```

3. 改用 putty 或 terminal 做SSH登入測試
   - Pass: 改用 terminal/putty 做後續操作
   - Fail: 重新檢查 Step 1. & 2.

### 重啟成功後, 即可使用 SSH 登入主機

1. 編輯 patch.sh 腳本,  編輯完後執行

```shell
vim patch.sh
sudo chmod 500 patch.sh
./patch.sh
```

- [patch.sh]([18])

2. 建立 grub 密碼, 會產出一份 所輸入密碼的 PBKDF2 hash 字串 [PW_PBKFD2]

```shell
grub-mkpasswd-pbkdf2
```

3. 編輯 /etc/grub.d/00_header
   將以下設定追加進 /etc/grub.d/00_header 最後面

```
cat << EOF
set superuser="admin"
password_pbkfd2 admin 
[PW_PBKFD2]
EOF
```

4. 增加 10.patch 腳本, 做 grub-patch
   建立 /etc/grub.d/10.patch

```shell
sudo su -
cd /etc/grub.d
patch -p0 -l < 10.patch
cp 10_linux 10_linux.orig
exit
```

- [10.patch]([19])

改變 10.patch 的權限
更新 grub2

```shell
sudo chmod 644 ./10_linux.orig
sudo update-grub2
```

## Finally configured

### 關閉 ssh 遠端密碼登入

```shell
sudo sed -i 's/PasswordAuthentication no/PasswordAuthentication yes/' /etc/ssh/sshd_config
```

### 關閉 cloud-init 網卡設定

```shell
sudo cloud-init clean
sudo rm  /etc/netplan/*
sudo shutdown -h now
```

### 關閉 /tmp 的執行權限

- [set-noexec.sh]([20])

### 移除 VM 的預設密碼

點選 Configure > Settings -> vApp Options, **Default User Password** 在最下方的 Properties 表格


| ![22] |
| ------- |

移除 **Default User Password**


| ![23] |
| ------- |

先將原始範本 <**gke-on-prem-ubuntu-1.6.5-gke.0**> 改 <**gke-on-prem-ubuntu-1.6.5-gke.0-origin**>
再將此 VM 轉換成新範本, 並重命名為 <**gke-on-prem-ubuntu-1.6.5-gke.0**>


| ![8] |
| ------ |

## CIS Checking List


| 項目 | 執行內容                                                                                                                                                                                                                                                                                                                                                                                         |
| ------ | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| 1    | "在/etc/login.defs檔案中設置PASS_MAX_DAYS 90 PASS_MIN_LEN 12 PASS_WARN_AGE 14 PASS_MIN_DAYS 7"                                                                                                                                                                                                                                                                                                   |
| 2    | "在/etc/security/pwquality.conf檔案中設置(RHEL 7以上適用)minlen=12 dcredit=-1 lcredit=-1 ucredit=-1 ocredit=-1 difok=3"                                                                                                                                                                                                                                                                          |
| 4    | 確認root是唯一使用UID 0的帳號                                                                                                                                                                                                                                                                                                                                                                    |
| 5    | 確認root帳號的GID為0                                                                                                                                                                                                                                                                                                                                                                             |
| 6    | 確認沒有重複的UID或GID存在                                                                                                                                                                                                                                                                                                                                                                       |
| 7    | 確認沒有重複的user name或group name存在                                                                                                                                                                                                                                                                                                                                                          |
| 8    | 停用guest及nobody帳號                                                                                                                                                                                                                                                                                                                                                                            |
| 9    | 檢視/etc/passwd檔案內容，確認UID小於500之非root系統帳號的登入shell皆設為「/sbin/nologin」                                                                                                                                                                                                                                                                                                        |
| 10   | 在每個ext4、ext3、ext2及xfs檔案格式之磁碟分割區(PART)執行下列指令將需要讓所有帳號都有寫入權限的目錄設定sticky bit<br/>#find PART -xdev -type d\( -perm -0002 -a ! -perm -1000 \) -print                                                                                                                                                                                                          |
| 11   | 確認每個帳號home目錄下的dot files不能被group或其它帳號寫入                                                                                                                                                                                                                                                                                                                                       |
| 12   | 確認每個帳號home目錄下都不存在 .forward、.netrc、.rhosts這三個檔案                                                                                                                                                                                                                                                                                                                               |
| 13   | 確認系統中不存在/etc/hosts.equiv、/etc/shosts.equiv檔案確認系統中不存在.shosts及.xhosts檔案 如需啟用，應經申請單位部室主管核可，並經資安人員審核通過啟用。                                                                                                                                                                                                                                       |
| 14   | 在每個ext4、ext3、ext2及xfs檔案格式之磁碟分割區(PART)執行下列指令確認所有檔案及目錄都有設置owner及group<br/>#find PART -xdev\( -nouser -o -nogroup \) -print                                                                                                                                                                                                                                     |
| 15   | 在/etc/ssh/sshd_config中設定下列參數:<br/>PermitRootLogin no <br/>PermitEmptyPasswords no<br/>ClientAliveInterval 300<br/>ClientAliveCountMax 3<br/>DenyGroups denysshg                                                                                                                                                                                                                          |
| 16   | 檢查/etc/default/useradd，應新增設定參數如下：<br/> INACTIVE=30                                                                                                                                                                                                                                                                                                                                  |
| 17   | 檢查/etc/profile，應設定:<br/>TMOUT=900<br/>在/etc/profile.d目錄下新增tmout.sh檔案，並在檔案增加以下內容： <br/>#!/bin/bash<br/>TMOUT=900 <br/>readonly TMOUT<br/>export TMOUT                                                                                                                                                                                                                   |
| 18   | 檢查/etc/profile之檔案權限設定，確認一般使用者無變更權限。                                                                                                                                                                                                                                                                                                                                       |
| 19   | "停用下列服務nfs、rpcbind、vsftpd、smb、rsh.socket、rlogin.socket、rexec.socket、telnet.socket、tftp.socket、rsyncd、rsh"                                                                                                                                                                                                                                                                        |
| 20   | "在每個ext4、ext3、ext2及xfs檔案格式之磁碟分割區(PART)執行下列指令確認不存在所有帳號都可寫入的檔案#find PART -xdev -type f -perm -0002 –print"                                                                                                                                                                                                                                                  |
| 21   | "確認/etc/logrotate.conf有設定: /var/log/wtmp、/var/log/secure及/var/log/messages的留存機制<br/>確認/etc/logrotate.d/syslog檔案，新增或修改成以下內容： <br/>/var/log/cron <br/>/var/log/maillog <br/>/var/log/spooler <br/>/var/log/boot.log"                                                                                                                                                   |
| 22   | "設定密碼保護本機開機載入程式GRUB <br/>$ cat /etc/grub.conf                                                                                                                                                                                                                                                                                                                                            |
| 23   | "正式環境最高權限帳號(root)之密碼管理區分二段，由兩位專責人員分別持有，並簽封存於機房保管設簿登記 或透過特權帳號系統機制管理"                                                                                                                                                                                                                                                                    |
| 24   | 建立獨立的/tmp分割磁區並設定為為nodev、nosuid、noexec                                                                                                                                                                                                                                                                                                                                            |
| 25   | "建立獨立的/var/tmp分割磁區並設定為nodev、nosuid、noexec或將/var/tmp綁定掛載到/tmp"                                                                                                                                                                                                                                                                                                              |
| 26   | 編輯/etc/fstab檔案，將/dev/shm設定為nodev、nosuid、noexec                                                                                                                                                                                                                                                                                                                                        |
| 27   | "限制創建檔案文件之預設權限在/etc/profile及/etc/bashrc加入umask 027"                                                                                                                                                                                                                                                                                                                             |
| 28   | "啟動系統稽核服務chkconfig auditd on chkconfig --list                                                                                                                                                                                                                                                                                                                                            |
| 29   | "設定系統重要稽核原則-記錄變更日期與時間事件編輯/etc/audit/audit.rules檔案，新增以下內容:<br/> -a always,exit -F arch=b64 -S adjtimex -S settimeofday -S stime -k time-change <br/>-a always,exit -F arch=b64 -S clock_settime -k time-change <br/>-w /etc/localtime -p wa -k time-change"                                                                                                       |
| 30   | "設定系統重要稽核原則-記錄不成功的未經授權檔案存取 編輯/etc/audit/audit.rules檔案，新增以下內容:<br/>  -a always,exit -F arch=b64 -S creat -S open -S openat -S truncate -S ftruncate -F exit=-EACCES -F auid>=500 -F auid!=4294967295 -k access  <br/>-a always,exit -F arch=b64 -S creat -S open -S openat -S truncate -S ftruncate -F exit=-EPERM -F auid>=500 -F auid!=4294967295 -k access" |
| 31   | "設定系統重要稽核原則-記錄特權指令使用情形執行下列指令，針對每個磁區(PART)的特權指令產生稽核規則：<br/> #find PART -xdev\( -perm -4000 -o -perm -2000 \) -type f                                                                                                                                                                                                                                 |
| 32   | 確保系統漏洞已獲得適當之管理與更新，若經評估後決定暫不修補之問題應確認其原因。                                                                                                                                                                                                                                                                                                                   |

---

[1]: ../Anthos/materials/compliance-1.png
[2]: ../Anthos/materials/compliance-2.png
[3]: ../Anthos/materials/compliance-3.png
[4]: ../Anthos/materials/compliance-4.png
[5]: ../Anthos/materials/compliance-5.png
[6]: ../Anthos/materials/compliance-6.png
[7]: ../Anthos/materials/compliance-7.png
[8]: ../Anthos/materials/compliance-8.png
[9]: ../Anthos/admin-cluster/admin-cluster.yaml
[10]: ../Anthos/admin-cluster/admin-cluster-hostconfig.yaml
[11]: ../Anthos/admin-cluster/admin-creds.yaml
[12]: ../Anthos/materials/create-vm-1.png
[13]: ../Anthos/materials/create-vm-2.png
[14]: ../Anthos/materials/create-vm-3.png
[15]: ../Anthos/materials/create-vm-4.png
[16]: ../Anthos/materials/create-vm-5.png
[17]: ../Compliance/fstab
[18]: ../../CathayBank/Anthos/patch.sh
[19]: ../../CathayBank/Anthos/10-patch.sh
[20]: ../../CathayBank/Anthos/set-noexec.sh
[21]: ../../CathayBank/Anthos/grub-pw
[22]: ../Anthos/materials/create-vm-6.png
[23]: ../Anthos/materials/create-vm-7.png
[24]: ../Anthos/materials/compliance-0.png
[25]: ../Anthos/materials/create-vm-0.png
